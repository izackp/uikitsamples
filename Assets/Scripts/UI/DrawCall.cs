﻿using UnityEngine;
using System.Collections.Generic;

public class DrawInfo {
	
	public enum Clipping
	{
		None,
		HardClip,	// Obsolete. Used to use clip() but it's not supported by some devices.
		AlphaClip,	// Adjust the alpha, compatible with all devices
		SoftClip,	// Alpha-based clipping with a softened edge
	}
	
	public List<Vector3> Verts;
	public List<Vector2> UVs;
	public List<Color32> Colors;
	public Clipping ClippingMode = Clipping.AlphaClip;
	public string ImageName;

	Vector4 _clipRange = new Vector4(0f, 0f, 100f, 100f);
	Vector2 _clipSoft;
	int[] _indices;
	
	public static DrawInfo BuildRect() {

		//TODO: Make read only
		List<Vector3> verts = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<UnityEngine.Color32> colors = new List<UnityEngine.Color32>();
		
		verts.Add(new Vector3(100f,  0f, 0f));
		verts.Add(new Vector3(100f, -100f, 0f));
		verts.Add(new Vector3(0f, -100f, 0f));
		verts.Add(new Vector3(0f,  0f, 0f));
		
		Rect mRect = new Rect(0f, 0f, 1f, 1f);
		uvs.Add(new Vector2(mRect.xMax, mRect.yMax));
		uvs.Add(new Vector2(mRect.xMax, mRect.yMin));
		uvs.Add(new Vector2(mRect.xMin, mRect.yMin));
		uvs.Add(new Vector2(mRect.xMin, mRect.yMax));
		
		Color32 col = Color.white;
		colors.Add(col);
		colors.Add(col);
		colors.Add(col);
		colors.Add(col);
		
		DrawInfo drawInfo = BuildCustomVerts(verts, uvs, colors);
		return drawInfo;
	}
	
	public static DrawInfo BuildCustomVerts(List<Vector3> verts, List<Vector2> uvs, List<Color32> colors) {
		
		DrawInfo drawInfo = new DrawInfo();
		drawInfo.SetVertsAndIndices(verts);
		drawInfo.UVs = uvs;
		drawInfo.Colors = colors;

		return drawInfo;
	}
	
	static Shader FindShader(string baseName, Clipping clippingMode) {
		string alpha = " (AlphaClip)";
		string soft	= " (SoftClip)";

		baseName = baseName.Replace(alpha, "");
		baseName = baseName.Replace(soft, "");

		if (clippingMode == Clipping.HardClip || clippingMode == Clipping.AlphaClip)
			return Shader.Find(baseName + alpha);
		
		if (clippingMode == Clipping.SoftClip) 
			return Shader.Find(baseName + soft);
		
		Debug.LogError(baseName + " doesn't have a clipped shader version for " + clippingMode);
		return null;
	}
	
	public static int[] BuildIndices(int vertCount) {
		// It takes 6 indices to draw a quad of 4 vertices
		int indexCount = (vertCount >> 1) * 3;
		int[] indices = new int[indexCount];
		int index = 0;
		
		for (int i = 0; i < vertCount; i += 4)
		{
			indices[index++] = i;
			indices[index++] = i + 1;
			indices[index++] = i + 2;
			
			indices[index++] = i + 2;
			indices[index++] = i + 3;
			indices[index++] = i;
		}
		
		return indices;
	}
	
	static Texture2D sCachedTex = null;
	public static Texture2D BlankTexture() {
		if (sCachedTex)
			return sCachedTex;
		
		Texture2D blankTex = new Texture2D(1, 1);
		blankTex.SetPixel(0, 0, Color.green);
		blankTex.wrapMode = TextureWrapMode.Repeat;
		blankTex.Apply();
		sCachedTex = blankTex;
		return sCachedTex;
	}

	public void SetVertsAndIndices(List<Vector3> verts, int[] indices = null) {
		int vertCount = verts.Count;
		Verts = verts;
		_indices = BuildIndices(vertCount);
	}

	public bool IsValid() {
		if (IsValidMaterial() == false)
			return false;

		if (IsValidMesh() == false)
			return false;

		return true;
	}

	public bool IsValidMaterial() {
		string shaderName = "Unlit/Transparent Colored";
		Shader shader = Shader.Find(shaderName);

		if (shader == null) {
			Debug.LogError("Cannot find shader: " + shaderName);
			return false;
		}

		Shader shaderWithClipping = FindShader(shader.name, ClippingMode);
		if (shaderWithClipping == null) {
			//error logged in FindShader
			return false;
		}

		if (GenerateTexture() == null) {
			Debug.LogError("Cannot generate the texture.");
			return false;
		}

		return true;
	}

	public bool IsValidMesh() {
		int vertCount = Verts.Count;
		if (vertCount != UVs.Count || vertCount != Colors.Count) {
			Debug.LogError("There are a different number of vertices then UVs and/or colors");
			return false;
		}

		if (vertCount >= 65000) {
			Debug.LogError("Too many vertices: " + vertCount);
			return false;
		}

		return true;
	}

	public Mesh GenerateMesh() {
		if (IsValidMesh() == false)
			return null;
		
		Mesh mesh = new Mesh();
		mesh.hideFlags = HideFlags.DontSave;
		mesh.name = "Mesh for DrawInfo";
		mesh.MarkDynamic();

		mesh.vertices = Verts.ToArray();
		mesh.triangles = _indices;
		mesh.uv = UVs.ToArray();
		//		mesh.normals = norms.ToArray();
		//		mesh.tangents = tans.ToArray();
		mesh.colors32 = Colors.ToArray();
		mesh.RecalculateBounds();
		return mesh;
	}

	public Material[] GenerateMaterials() {
		Shader shader = Shader.Find("Unlit/Transparent Colored");
		Shader shaderWithClipping = FindShader(shader.name, ClippingMode);
		
		Material mat = new Material(shaderWithClipping);
		mat.hideFlags = HideFlags.DontSave;
		mat.mainTexture = GenerateTexture();
		mat.shader = shaderWithClipping;
		
		mat.mainTextureOffset = new Vector2(-_clipRange.x / _clipRange.z, -_clipRange.y / _clipRange.w);
		mat.mainTextureScale = new Vector2(1f / _clipRange.z, 1f / _clipRange.w);
		
		Vector2 sharpness = new Vector2(1000.0f, 1000.0f);
		mat.SetVector("_ClipSharpness", sharpness);

		return new [] {mat};
	}

	Texture GenerateTexture() {
		if (ImageName != null)
			return Resources.Load("brick") as Texture;

		return BlankTexture();
	}
}

public class DrawCall : MonoBehaviour {
	
	MeshFilter		mFilter;
	MeshRenderer	mRenderer;
	DrawInfo		_drawInfo;
	
	void Start () {
		mFilter = gameObject.AddComponent<MeshFilter>();
		mRenderer = gameObject.AddComponent<MeshRenderer>();
		_drawInfo = DrawInfo.BuildRect();
		_drawInfo.ImageName = "brick";

		if (_drawInfo.IsValid() == false) {
			Debug.LogError("Aborting Draw Call.");
			return;
		}

		mFilter.mesh = _drawInfo.GenerateMesh ();
		mRenderer.sharedMaterials = _drawInfo.GenerateMaterials();
	}
}

