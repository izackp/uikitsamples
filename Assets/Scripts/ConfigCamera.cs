﻿using UnityEngine;

public class ConfigCamera : MonoBehaviour {
	
	public float fZoom = 0;
	float pixelsPerUnit = 1.0f;
	public float ScreenHeight;
	public float ScreenWidth;
	
	void LateUpdate () {
		//pixel perfect = (Screen.height * 0.5f) / 100
		ScreenHeight = Screen.height;
		ScreenWidth = Screen.width;
		fZoom += (Input.GetAxis("Mouse ScrollWheel") * 5.0f);
		
		float s_baseOrthographicSize = (Screen.height * 0.5f + fZoom)/ pixelsPerUnit;
		s_baseOrthographicSize = s_baseOrthographicSize - (s_baseOrthographicSize % 2);
		Camera.main.orthographicSize = s_baseOrthographicSize;
	}

	public void OffsetZoom (float magnitude) {
		fZoom += magnitude;
	}
}